document.addEventListener("deviceready", onDeviceReady, false);
var my_media = null;

var db = window.openDatabase("quiz", "1.0", "e-Tudlo App", 200000);

function onDeviceReady() {
    flush_localstorage(); 

    local_Lesson1();
    local_lesson2();
    local_lesson4();
}


function playAudio() {
    // Play the audio file at url
    var my_media = new Media('/android_asset/www/music/bgm2.mp3',
        // success callback
        function () { console.log("playAudio():Audio Success"); },
        // error callback
        function (err) { console.log("playAudio():Audio Error: " + err); },
        function (status) { 
            if(status==Media.MEDIA_STOPPED ) {
                my_media.play();
            }; 
        }
    );

    // Play audio
    my_media.play();
    my_media.setVolume('0.2');
}

function stopAudio() {
    if (my_media) {
        my_media.stop();
    }
}

function playWrongAudio() {
    // Play the audio file at url
    var my_media = null;
    var my_media = new Media('/android_asset/www/music/wrong.wav',
        // success callback
        function () { console.log("playAudio():playWrongAudio Success"); },
        // error callback
        function (err) { console.log("playAudio():playWrongAudio Error: " + err); },
        function (status) { 
            if(status==Media.MEDIA_STOPPED ) {
                my_media.stop();
            }; 
        }
    );

    // Play audio
    my_media.play();
    my_media.setVolume('1.0');
}


function playCorrectAudio() {
    var my_media = null;
    var my_media = new Media('/android_asset/www/music/smb_coin.wav',
        // success callback
        function () { console.log("playAudio():playCorrectAudio Success"); },
        // error callback
        function (err) { console.log("playAudio():playCorrectAudio Error: " + err); },
        function (status) { 
            if(status==Media.MEDIA_STOPPED ) {
                my_media.stop();
            }; 
        }
    );

    // Play audio
    my_media.play();
    my_media.setVolume('1.0');
}



function closeApp(){
    console.log('closeApp');
    stopAudio();
    navigator.app.exitApp();

} 

function flush_localstorage(){
    localStorage.removeItem('lesson1-1');
    localStorage.removeItem('lesson1-2');
    localStorage.removeItem('lesson1-3');
    localStorage.removeItem('lesson1-4');
    localStorage.removeItem('lesson1-5');
    localStorage.removeItem('lesson1-6');
    localStorage.removeItem('lesson1-7');
    localStorage.removeItem('lesson1-8');

    localStorage.removeItem('lesson2-1');
    localStorage.removeItem('lesson2-2');
    localStorage.removeItem('lesson2-3');
    localStorage.removeItem('lesson2-4');
    localStorage.removeItem('lesson2-5');

    localStorage.removeItem('lesson4-1');
    localStorage.removeItem('lesson4-2');
    localStorage.removeItem('lesson4-3');
    localStorage.removeItem('lesson4-4');
    localStorage.removeItem('lesson4-5');
    localStorage.removeItem('lesson4-6');
    localStorage.removeItem('lesson4-7');
    localStorage.removeItem('lesson4-8');

}

function local_Lesson1(){
    localStorage.setItem('lesson1-1','1');
    localStorage.setItem('lesson1-2','9');
    localStorage.setItem('lesson1-3','3');
    localStorage.setItem('lesson1-4','8');
    localStorage.setItem('lesson1-5','6');
    localStorage.setItem('lesson1-6','2');
    localStorage.setItem('lesson1-7','4');
    localStorage.setItem('lesson1-8','6');
}

function local_lesson2(){
    localStorage.setItem('lesson2-1','5');
    localStorage.setItem('lesson2-2','8');
    localStorage.setItem('lesson2-3','9');
    localStorage.setItem('lesson2-4','7');
    localStorage.setItem('lesson2-5','9');
}

function local_lesson4(){
    localStorage.setItem('lesson4-1','3');
    localStorage.setItem('lesson4-2','1');
    localStorage.setItem('lesson4-3','6');
    localStorage.setItem('lesson4-4','4');
    localStorage.setItem('lesson4-5','2');
    localStorage.setItem('lesson4-6','4');
    localStorage.setItem('lesson4-7','3');
    localStorage.setItem('lesson4-8','3');
}





