var gameArr = new Array();
var score = 0;
var indexquestion = 0;
var countAnswer = 0;
var category = null;


function home(){
  window.location.href="index.html";
}

function lesson(){
  window.location.href="index.html#lesson";
}

function lesson1(){
  window.location.href="lesson1.html";
}

function lesson2(){
  window.location.href="lesson2.html";
}

function lesson3(){
  window.location.href="lesson3.html";
}

function lesson4(){
  window.location.href="lesson4.html";
}

function lesson5(){
  window.location.href="lesson5.html";
}


function lesson1_quiz(){
  window.location.href= "lesson1_quiz1.html";
}

function lesson2_quiz(){
  window.location.href= "lesson2_quiz2.html";
}


function lesson4_quiz(){
  window.location.href= "lesson4_quiz4.html";
}




$(document).on( "click","#btn_exercise_volume", function() {
   // window.plugins.tts.speak(document.getElementById('lesson_detail_content').text);
   	console.log('window.plugins.tts.startup(doSpeak, errHandler);');
    window.plugins.tts.startup(doSpeak, errHandler);
});

function doSpeak() {
    window.plugins.tts.speak($('#exercise_detail_content').text(), {} , errHandler);
}


$(document).on( "click","#btn_correct_volume", function() {
   // window.plugins.tts.speak(document.getElementById('lesson_detail_content').text);
    console.log('window.plugins.tts.startup(correctSpeak, errHandler);');
    window.plugins.tts.startup(correctSpeak, errHandler);
});

function correctSpeak() {
    window.plugins.tts.speak($('#result_question').text(), {} , errHandler);
}

$(document).on( "click","#btn_wrong_volume", function() {
   // window.plugins.tts.speak(document.getElementById('lesson_detail_content').text);
    console.log('window.plugins.tts.startup(wrongSpeak, errHandler);');
    window.plugins.tts.startup(wrongSpeak, errHandler);
});

function wrongSpeak() {
    window.plugins.tts.speak($('#wrong_result_question').text(), {} , errHandler);
}


function alertDismissed() {
    // do something
}


function submit_lesson1(){

    var lesson1_1_answer = localStorage.getItem("lesson1-1");
    var lesson1_2_answer = localStorage.getItem("lesson1-2");
    var lesson1_3_answer = localStorage.getItem("lesson1-3");
    var lesson1_4_answer = localStorage.getItem("lesson1-4");
    var lesson1_5_answer = localStorage.getItem("lesson1-5");
    var lesson1_6_answer = localStorage.getItem("lesson1-6");
    var lesson1_7_answer = localStorage.getItem("lesson1-7");
    var lesson1_8_answer = localStorage.getItem("lesson1-8");

    var my_lesson1_1 = $('#question1').val();
    var my_lesson1_2 = $('#question2').val();
    var my_lesson1_3 = $('#question3').val();
    var my_lesson1_4 = $('#question4').val();
    var my_lesson1_5 = $('#question5').val();
    var my_lesson1_6 = $('#question6').val();
    var my_lesson1_7 = $('#question7').val();
    var my_lesson1_8 = $('#question8').val();

    if (my_lesson1_1 == '') {
      navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;
    }else if (my_lesson1_2 == '') {
       navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;
    }else if (my_lesson1_3 == '') {
       navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;      
    }else if (my_lesson1_4 == '') {
       navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;
    }else if (my_lesson1_5 == '') {
       navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;
    }else if (my_lesson1_6 == '') {
       navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;
    }else if (my_lesson1_7 == '') {
       navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;
    }else if (my_lesson1_8 == '') {
       navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;
    }


    $('#popupDialog').popup('open');

    $(document).on("click","#btn_submit_yes", function() {

        $("#my_lesson1_1").text(my_lesson1_1);
        $("#my_lesson1_2").text(my_lesson1_2);
        $("#my_lesson1_3").text(my_lesson1_3);
        $("#my_lesson1_4").text(my_lesson1_4);
        $("#my_lesson1_5").text(my_lesson1_5);
        $("#my_lesson1_6").text(my_lesson1_6);
        $("#my_lesson1_7").text(my_lesson1_7);
        $("#my_lesson1_8").text(my_lesson1_8);

        $("#ans_lesson1_1").text(lesson1_1_answer);
        $("#ans_lesson1_2").text(lesson1_2_answer);
        $("#ans_lesson1_3").text(lesson1_3_answer);
        $("#ans_lesson1_4").text(lesson1_4_answer);
        $("#ans_lesson1_5").text(lesson1_5_answer);
        $("#ans_lesson1_6").text(lesson1_6_answer);
        $("#ans_lesson1_7").text(lesson1_7_answer);
        $("#ans_lesson1_8").text(lesson1_8_answer);


        if(lesson1_1_answer == my_lesson1_1){
          console.log('answer: ' + lesson1_1_answer + ' trueanswer:' + my_lesson1_1);
          score  = score  + 1;
           $("#my_lesson1_1").addClass('correct');
        }else{
           $("#my_lesson1_1").addClass('wrong');
        }

        if(lesson1_2_answer == my_lesson1_2){
          console.log('answer: ' + lesson1_2_answer + ' trueanswer:' + my_lesson1_2);
          score  = score  + 1;
          $("#my_lesson1_2").addClass('correct');
        }else{
          $("#my_lesson1_2").addClass('wrong');
        }

        if(lesson1_3_answer == my_lesson1_3){
          console.log('answer: ' + lesson1_3_answer + ' trueanswer:' + my_lesson1_3);
          score  = score  + 1;
           $("#my_lesson1_3").addClass('correct');
        }else{
           $("#my_lesson1_3").addClass('wrong');
        }

        if(lesson1_4_answer == my_lesson1_4){
          console.log('answer: ' + lesson1_4_answer + ' trueanswer:' + my_lesson1_4);
          score  = score  + 1;
           $("#my_lesson1_4").addClass('correct');
        }else{
           $("#my_lesson1_4").addClass('wrong');
        }

        if(lesson1_5_answer == my_lesson1_5){
          console.log('answer: ' + lesson1_5_answer + ' trueanswer:' + my_lesson1_5);
          score  = score  + 1;
           $("#my_lesson1_5").addClass('correct');
        }else{
           $("#my_lesson1_5").addClass('wrong');
        }

        if(lesson1_6_answer == my_lesson1_6){
          console.log('answer: ' + lesson1_6_answer + ' trueanswer:' + my_lesson1_6);
          score  = score  + 1;
           $("#my_lesson1_6").addClass('correct');
        }else{
           $("#my_lesson1_6").addClass('wrong');
        }

        if(lesson1_7_answer == my_lesson1_7){
          console.log('answer: ' + lesson1_7_answer + ' trueanswer:' + my_lesson1_7);
          score  = score  + 1;
           $("#my_lesson1_7").addClass('correct');
        }else{
           $("#my_lesson1_7").addClass('wrong');
        }

        if(lesson1_8_answer == my_lesson1_8){
          console.log('answer: ' + lesson1_8_answer + ' trueanswer:' + my_lesson1_8);
          score  = score  + 1;
           $("#my_lesson1_8").addClass('correct');
        }else{
           $("#my_lesson1_8").addClass('wrong');
        }

        $("#lesson1_score").text(score);
        $.mobile.changePage($("#result"));

        event.stopPropagation();
        event.preventDefault();

        return false;
    });

    $(document).on("click","#btn_submit_no", function() {
        console.log('popupDialog-close');
       $('#popupDialog').popup('close');
    });  
};


function submit_lesson2(){
    var lesson1_1_answer = localStorage.getItem("lesson2-1");
    var lesson1_2_answer = localStorage.getItem("lesson2-2");
    var lesson1_3_answer = localStorage.getItem("lesson2-3");
    var lesson1_4_answer = localStorage.getItem("lesson2-4");
    var lesson1_5_answer = localStorage.getItem("lesson2-5");

    var my_lesson1_1 = $('#question1').val();
    var my_lesson1_2 = $('#question2').val();
    var my_lesson1_3 = $('#question3').val();
    var my_lesson1_4 = $('#question4').val();
    var my_lesson1_5 = $('#question5').val();

    if (my_lesson1_1 == '') {
      navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;
    }else if (my_lesson1_2 == '') {
       navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;
    }else if (my_lesson1_3 == '') {
       navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;      
    }else if (my_lesson1_4 == '') {
       navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;
    }else if (my_lesson1_5 == '') {
       navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;
    }


    $('#popupDialog').popup('open');

    $(document).on("click","#btn_submit_yes", function() {

        $("#my_lesson2_1").text(my_lesson1_1);
        $("#my_lesson2_2").text(my_lesson1_2);
        $("#my_lesson2_3").text(my_lesson1_3);
        $("#my_lesson2_4").text(my_lesson1_4);
        $("#my_lesson2_5").text(my_lesson1_5);

        $("#ans_lesson2_1").text(lesson1_1_answer);
        $("#ans_lesson2_2").text(lesson1_2_answer);
        $("#ans_lesson2_3").text(lesson1_3_answer);
        $("#ans_lesson2_4").text(lesson1_4_answer);
        $("#ans_lesson2_5").text(lesson1_5_answer);


        if(lesson1_1_answer == my_lesson1_1){
          console.log('answer: ' + lesson1_1_answer + ' trueanswer:' + my_lesson1_1);
          score  = score  + 1;
           $("#my_lesson2_1").addClass('correct');
        }else{
           $("#my_lesson2_1").addClass('wrong');
        }

        if(lesson1_2_answer == my_lesson1_2){
          console.log('answer: ' + lesson1_2_answer + ' trueanswer:' + my_lesson1_2);
          score  = score  + 1;
          $("#my_lesson2_2").addClass('correct');
        }else{
          $("#my_lesson2_2").addClass('wrong');
        }

        if(lesson1_3_answer == my_lesson1_3){
          console.log('answer: ' + lesson1_3_answer + ' trueanswer:' + my_lesson1_3);
          score  = score  + 1;
           $("#my_lesson2_3").addClass('correct');
        }else{
           $("#my_lesson2_3").addClass('wrong');
        }

        if(lesson1_4_answer == my_lesson1_4){
          console.log('answer: ' + lesson1_4_answer + ' trueanswer:' + my_lesson1_4);
          score  = score  + 1;
           $("#my_lesson2_4").addClass('correct');
        }else{
           $("#my_lesson2_4").addClass('wrong');
        }

        if(lesson1_5_answer == my_lesson1_5){
          console.log('answer: ' + lesson1_5_answer + ' trueanswer:' + my_lesson1_5);
          score  = score  + 1;
           $("#my_lesson2_5").addClass('correct');
        }else{
           $("#my_lesson2_5").addClass('wrong');
        }

        

        $("#lesson2_score").text(score);
        $.mobile.changePage($("#result"));

        event.stopPropagation();
        event.preventDefault();

        return false;
    });

    $(document).on("click","#btn_submit_no", function() {
        console.log('popupDialog-close');
       $('#popupDialog').popup('close');
    });  
};


function submit_lesson4(){
    var lesson4_1_answer = localStorage.getItem("lesson4-1");
    var lesson4_2_answer = localStorage.getItem("lesson4-2");
    var lesson4_3_answer = localStorage.getItem("lesson4-3");
    var lesson4_4_answer = localStorage.getItem("lesson4-4");
    var lesson4_5_answer = localStorage.getItem("lesson4-5");
    var lesson4_6_answer = localStorage.getItem("lesson4-6");
    var lesson4_7_answer = localStorage.getItem("lesson4-7");
    var lesson4_8_answer = localStorage.getItem("lesson4-8");

    var my_lesson4_1 = $('#question1').val();
    var my_lesson4_2 = $('#question2').val();
    var my_lesson4_3 = $('#question3').val();
    var my_lesson4_4 = $('#question4').val();
    var my_lesson4_5 = $('#question5').val();
    var my_lesson4_6 = $('#question6').val();
    var my_lesson4_7 = $('#question7').val();
    var my_lesson4_8 = $('#question8').val();

    if (my_lesson4_1 == '') {
      navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;
    }else if (my_lesson4_2 == '') {
       navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;
    }else if (my_lesson4_3 == '') {
       navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;      
    }else if (my_lesson4_4 == '') {
       navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;
    }else if (my_lesson4_5 == '') {
       navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;
    }else if (my_lesson4_6 == '') {
       navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;
    }else if (my_lesson4_7 == '') {
       navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;
    }else if (my_lesson4_8 == '') {
       navigator.notification.alert(
          'Please provide answer!',  // message
          alertDismissed,         // callback
          'Empty answer',            // title
          'Ok'                  // buttonName
      );
      return false;
    }


    $('#popupDialog').popup('open');

    $(document).on("click","#btn_submit_yes", function() {

        $("#my_lesson4_1").text(my_lesson4_1);
        $("#my_lesson4_2").text(my_lesson4_2);
        $("#my_lesson4_3").text(my_lesson4_3);
        $("#my_lesson4_4").text(my_lesson4_4);
        $("#my_lesson4_5").text(my_lesson4_5);
        $("#my_lesson4_6").text(my_lesson4_6);
        $("#my_lesson4_7").text(my_lesson4_7);
        $("#my_lesson4_8").text(my_lesson4_8);

        $("#ans_lesson4_1").text(lesson4_1_answer);
        $("#ans_lesson4_2").text(lesson4_2_answer);
        $("#ans_lesson4_3").text(lesson4_3_answer);
        $("#ans_lesson4_4").text(lesson4_4_answer);
        $("#ans_lesson4_5").text(lesson4_5_answer);
        $("#ans_lesson4_6").text(lesson4_6_answer);
        $("#ans_lesson4_7").text(lesson4_7_answer);
        $("#ans_lesson4_8").text(lesson4_8_answer);


        if(lesson4_1_answer == my_lesson4_1){
          console.log('answer: ' + lesson4_1_answer + ' trueanswer:' + my_lesson4_1);
          score  = score  + 1;
           $("#my_lesson4_1").addClass('correct');
        }else{
           $("#my_lesson4_1").addClass('wrong');
        }

        if(lesson4_2_answer == my_lesson4_2){
          console.log('answer: ' + lesson4_2_answer + ' trueanswer:' + my_lesson4_2);
          score  = score  + 1;
          $("#my_lesson4_2").addClass('correct');
        }else{
          $("#my_lesson4_2").addClass('wrong');
        }

        if(lesson4_3_answer == my_lesson4_3){
          console.log('answer: ' + lesson4_3_answer + ' trueanswer:' + my_lesson4_3);
          score  = score  + 1;
           $("#my_lesson4_3").addClass('correct');
        }else{
           $("#my_lesson4_3").addClass('wrong');
        }

        if(lesson4_4_answer == my_lesson4_4){
          console.log('answer: ' + lesson4_4_answer + ' trueanswer:' + my_lesson4_4);
          score  = score  + 1;
           $("#my_lesson4_4").addClass('correct');
        }else{
           $("#my_lesson4_4").addClass('wrong');
        }

        if(lesson4_5_answer == my_lesson4_5){
          console.log('answer: ' + lesson4_5_answer + ' trueanswer:' + my_lesson4_5);
          score  = score  + 1;
           $("#my_lesson4_5").addClass('correct');
        }else{
           $("#my_lesson4_5").addClass('wrong');
        }

        if(lesson4_6_answer == my_lesson4_6){
          console.log('answer: ' + lesson4_6_answer + ' trueanswer:' + my_lesson4_6);
          score  = score  + 1;
           $("#my_lesson4_6").addClass('correct');
        }else{
           $("#my_lesson4_6").addClass('wrong');
        }

        if(lesson4_7_answer == my_lesson4_7){
          console.log('answer: ' + lesson4_7_answer + ' trueanswer:' + my_lesson4_7);
          score  = score  + 1;
           $("#my_lesson4_7").addClass('correct');
        }else{
           $("#my_lesson4_7").addClass('wrong');
        }

        if(lesson4_8_answer == my_lesson4_8){
          console.log('answer: ' + lesson4_8_answer + ' trueanswer:' + my_lesson4_8);
          score  = score  + 1;
           $("#my_lesson4_8").addClass('correct');
        }else{
           $("#my_lesson4_8").addClass('wrong');
        }

        $("#lesson4_score").text(score);
        $.mobile.changePage($("#result"));

        event.stopPropagation();
        event.preventDefault();

        return false;
    });

    $(document).on("click","#btn_submit_no", function() {
        console.log('popupDialog-close');
       $('#popupDialog').popup('close');
    });  
};





